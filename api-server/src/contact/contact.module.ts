import { Module } from '@nestjs/common';
import { ContactService } from './contact.service';
import { ContactController } from './contact.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ContactFactory } from './entities/contact.entity';
import { UserFactory } from 'src/user/entities/user.entity';

@Module({
  imports: [MongooseModule.forFeature([ContactFactory, UserFactory])],
  controllers: [ContactController],
  providers: [ContactService],
  exports: [ContactService],
})
export class ContactModule {}
