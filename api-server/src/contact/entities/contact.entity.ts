import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { User } from 'src/user/entities/user.entity';

export type ContactDocument = HydratedDocument<Contact>;

@Schema({ timestamps: true })
export class Contact {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  user_1: User;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  use_2: User;

  @Prop()
  chat_background_url: string;

  @Prop()
  nickname_1: string;

  @Prop()
  nickname_2: string;
}

export const ContactFactory: ModelDefinition = {
  name: Contact.name,
  schema: SchemaFactory.createForClass(Contact),
};
