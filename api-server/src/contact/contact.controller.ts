import { Controller, Get, Query, Request, UseGuards } from '@nestjs/common';
import { AccessTokenGuard } from 'src/authentication/guards/accessToken.guard';
import { BaseController } from 'src/base.controller';
import { Contact } from './entities/contact.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/user/entities/user.entity';

@Controller('contacts')
export class ContactController extends BaseController<Contact> {
  constructor(
    @InjectModel(Contact.name)
    private readonly contactModel: Model<Contact>,
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
  ) {
    super(contactModel);
  }

  @UseGuards(AccessTokenGuard)
  @Get('search')
  async searchContact(@Request() request, @Query() query) {}
}
