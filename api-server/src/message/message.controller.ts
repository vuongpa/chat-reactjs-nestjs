import { Controller, Get, Query, Request, UseGuards } from '@nestjs/common';
import { BaseController } from 'src/base.controller';
import { Message } from './entities/message.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AccessTokenGuard } from 'src/authentication/guards/accessToken.guard';
import { Utils } from 'utils';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;

@Controller('messages')
export class MessageController extends BaseController<Message> {
  constructor(
    @InjectModel(Message.name)
    private readonly messageModel: Model<Message>,
  ) {
    super(messageModel);
  }

  @UseGuards(AccessTokenGuard)
  @Get()
  async getMessage(@Request() request, @Query() query) {
    const { filter: filterJson, limit = 20, skip = 0 } = query;
    const filter = Utils.jsonValid(filterJson) ? JSON.parse(filterJson) : {};
    const participantId = filter?.participantId;
    const currentUserId = request.user.sub;

    if (!participantId) {
      return [];
    }

    const messages = await this.messageModel
      .find(
        {
          sender: {
            $in: [new ObjectId(currentUserId), new ObjectId(participantId)],
          },
          receiver: {
            $in: [new ObjectId(currentUserId), new ObjectId(participantId)],
          },
        },
        null,
        {
          limit,
          skip,
          populate: ['sender', 'receiver'],
          sort: { messageId: -1 },
        },
      )
      .lean();
    return messages.reverse();
  }
}
