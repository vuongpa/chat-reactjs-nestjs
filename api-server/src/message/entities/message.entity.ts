import {
  AsyncModelFactory,
  Prop,
  Schema,
  SchemaFactory,
  getConnectionToken,
} from '@nestjs/mongoose';
import mongoose, { Connection } from 'mongoose';
import { Conversation } from 'src/conversation/entities/conversation.entity';
import { User } from 'src/user/entities/user.entity';

@Schema({ timestamps: true })
export class Message {
  @Prop()
  messageId: number;

  @Prop({ required: true })
  text: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  sender: User;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  receiver: User;

  @Prop()
  status: string;

  @Prop()
  conversationId: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Conversation.name,
  })
  conversation: Conversation;
}

export const MessageFactory: AsyncModelFactory = {
  name: Message.name,
  useFactory: (connection: Connection) => {
    const MessageSchema = SchemaFactory.createForClass(Message);

    MessageSchema.pre<Message>('save', async function () {
      const lastMessage = await connection.db
        .collection('messages')
        .findOne({}, { sort: { _id: -1 }, projection: { messageId: true } });
      const messageId = lastMessage?.messageId ? ++lastMessage.messageId : 1;
      this.messageId = messageId;
    });

    return MessageSchema;
  },
  inject: [getConnectionToken()],
};
