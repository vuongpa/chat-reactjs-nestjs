import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Message } from './entities/message.entity';
import { Model } from 'mongoose';

@Injectable()
export class MessageService {
  constructor(
    @InjectModel(Message.name)
    private readonly messageModel: Model<Message>,
  ) {}

  async findLastMessage(currentUserId: any, participantId: any): Promise<any> {
    return this.messageModel.findOne(
      {
        $or: [
          {
            sender: {
              $nin: [currentUserId, participantId],
            },
          },
          {
            receiver: {
              $nin: [currentUserId, participantId],
            },
          },
        ],
      },
      null,
      { sort: { messageId: -1 }, populate: ['sender', 'receiver'] },
    );
  }
}
