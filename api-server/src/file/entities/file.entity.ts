import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { User } from 'src/user/entities/user.entity';

export type FileDocument = HydratedDocument<File>;

@Schema({ timestamps: true })
export class File {
  @Prop({
    required: true,
  })
  url: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  createdBy: User;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  size: number;
}

export const FileFactory: ModelDefinition = {
  name: File.name,
  schema: SchemaFactory.createForClass(File),
};
