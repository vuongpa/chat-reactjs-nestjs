import {
  Controller,
  Request,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Post } from '@nestjs/common';
import { FileService } from './file.service';
import { AccessTokenGuard } from 'src/authentication/guards/accessToken.guard';

@Controller('files')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @UseGuards(AccessTokenGuard)
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @UploadedFile()
    file: Express.Multer.File,
    @Request() request,
  ) {
    return this.fileService.create(file, request.user);
  }
}
