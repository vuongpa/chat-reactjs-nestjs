import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { File } from './entities/file.entity';
import { Model } from 'mongoose';

@Injectable()
export class FileService {
  constructor(
    @InjectModel(File.name)
    private readonly fileModel: Model<File>,
  ) {}
  getFileUrl(file: Express.Multer.File) {
    return `http://localhost:3000/${file.filename}`;
  }

  async create(file: Express.Multer.File, user) {
    return this.fileModel.create({
      name: file.filename,
      size: file.size,
      url: this.getFileUrl(file),
      createdBy: user.sub,
    });
  }
}
