import {
  WebSocketGateway,
  SubscribeMessage,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { ChatService } from './chat.service';
import { Socket } from 'socket.io';

@WebSocketGateway({
  cors: {
    origin: 'http://localhost:8080',
  },
})
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(private readonly chatService: ChatService) {}

  async handleConnection(client: Socket) {
    console.log(`${client.id} connected...`);
    await this.chatService.clientConnected(client);
  }

  async handleDisconnect(client: Socket) {
    console.log(`${client.id} disconnected...`);
    await this.chatService.clientDisconnected(client);
  }

  @SubscribeMessage('send_message')
  async emitMessage(client: Socket, data: any) {
    const { text, sender, conversation, receiver } = data;
    if (sender) {
      await this.chatService.handleOneToOneMessage(client, {
        text,
        sender,
        receiver,
      });
    }

    if (conversation) {
      await this.chatService.handleConversationMessage(client, {
        text,
        conversation,
      });
    }
  }
}
