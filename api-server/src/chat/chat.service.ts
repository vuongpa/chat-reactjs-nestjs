import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/user/entities/user.entity';
import { Model } from 'mongoose';
import { Message } from 'src/message/entities/message.entity';
import { Device } from 'src/device/entities/device.entity';
import { Socket } from 'socket.io';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { MessageService } from 'src/message/message.service';
import { UserService } from 'src/user/user.service';
import { isEmpty, omit } from 'lodash';
import { ConversationMessage, OneToOneMessage } from './chat.dto';

@Injectable()
export class ChatService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    @InjectModel(Message.name)
    private readonly messageModel: Model<Message>,
    @InjectModel(Device.name)
    private readonly deviceModel: Model<Device>,
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly messageService: MessageService,
  ) {}

  async getUserBySocket(client: Socket) {
    try {
      const jwtToken = client.handshake.headers.authorization;
      if (!jwtToken) {
        return null;
      }

      const jwtPayload = await this.jwtService.verifyAsync(jwtToken, {
        secret: this.configService.get('JWT_ACCESS_SECRET'),
      });

      return this.userModel.findById(jwtPayload.sub);
    } catch (err) {
      console.log('getUserBySocket error: ', err);
      return null;
    }
  }

  async clientConnected(client: Socket) {
    const user: any = await this.getUserBySocket(client);
    if (!user) {
      return;
    }

    const device = await this.deviceModel.create({
      deviceToken: client.id,
    });
    await this.userService.addDevice({
      userId: user.id,
      deviceId: device.id,
    });
  }

  async clientDisconnected(client: Socket) {
    const device = await this.deviceModel.findOne({
      filter: {
        deviceToken: client.id,
      },
    });
    const user: any = await this.getUserBySocket(client);

    if (!device || !user) {
      return;
    }

    await this.userService.removeDevice({
      userId: user.id,
      deviceId: device.id,
    });
    await this.deviceModel.deleteOne({
      id: device.id,
    });
  }

  async handleOneToOneMessage(client: Socket, messageData: OneToOneMessage) {
    const [sender, receiver] = await Promise.all([
      this.userModel.findById(messageData.sender),
      this.userModel
        .findById(messageData.receiver, null, {
          populate: ['devices'],
        })
        .lean(),
    ]);
    if (isEmpty(sender) || isEmpty(receiver)) {
      return;
    }

    const deviceTokens = receiver.devices.map((device) => device.deviceToken);
    if (isEmpty(deviceTokens)) {
      return;
    }

    const conversationId = [sender._id, receiver._id].sort().join('');
    const newMessage = await this.messageModel.create({
      ...messageData,
      conversationId,
    });

    deviceTokens.forEach((deviceToken) => {
      if (!deviceToken) {
        return;
      }

      client.to(deviceToken).emit('receive_message', {
        from: omit(sender, 'devices'),
        message: newMessage,
      });
    });
  }

  async handleConversationMessage(
    client: Socket,
    { text, conversation }: ConversationMessage,
  ) {}
}
