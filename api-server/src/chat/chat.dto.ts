import { IsString } from 'class-validator';

export class OneToOneMessage {
  @IsString()
  text: string;

  @IsString()
  sender: string;

  @IsString()
  receiver: string;
}

export class ConversationMessage {
  @IsString()
  text: string;

  @IsString()
  conversation: string;
}
