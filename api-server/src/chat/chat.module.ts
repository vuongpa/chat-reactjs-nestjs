import { Module } from '@nestjs/common';
import { ChatService } from './chat.service';
import { ChatGateway } from './chat.gateway';
import { MongooseModule } from '@nestjs/mongoose';
import { UserFactory } from 'src/user/entities/user.entity';
import { DeviceFactory } from 'src/device/entities/device.entity';
import { MessageFactory } from 'src/message/entities/message.entity';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from 'src/user/user.module';
import { DeviceModule } from 'src/device/device.module';
import { MessageModule } from 'src/message/message.module';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    MongooseModule.forFeature([UserFactory, DeviceFactory]),
    MongooseModule.forFeatureAsync([MessageFactory]),
    ConfigModule,
    UserModule,
    DeviceModule,
    MessageModule,
    JwtModule,
  ],
  providers: [ChatGateway, ChatService],
})
export class ChatModule {}
