import { ModelDefinition, SchemaFactory } from '@nestjs/mongoose';

export class Conversation {}

export const conversationFactory: ModelDefinition = {
  name: Conversation.name,
  schema: SchemaFactory.createForClass(Conversation),
};
