import { IsString, Length, Matches } from 'class-validator';

export class StartDto {
  @Matches(/^[a-zA-Z0-9]{4,10}$/, {
    message: 'Tên người dùng không được chứa ký tự đặc biệt',
  })
  username: string;
}

export class LoginDto {
  @IsString()
  username: string;

  @Length(6)
  password: string;
}
