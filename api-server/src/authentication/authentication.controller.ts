import {
  Body,
  Controller,
  Get,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { LoginDto, StartDto } from './authentication.dto';
import { RefreshTokenGuard } from './guards/refreshToken.guard';
import { AccessTokenGuard } from './guards/accessToken.guard';

@Controller('auth')
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService) {}

  @Post('start')
  async start(@Body() requestBody: StartDto) {
    return this.authenticationService.start(requestBody);
  }

  @UseGuards(RefreshTokenGuard)
  @Get('refresh')
  async refreshToken(@Request() request) {
    return this.authenticationService.refreshJwtTokens(request.user.sub);
  }

  @UseGuards(AccessTokenGuard)
  @Get('/me')
  async getCurrentUser(@Request() request) {
    return this.authenticationService.getCurrentUser(request.user.sub);
  }

  @UseGuards(AccessTokenGuard)
  @Put('/reset-password')
  async resetPassword(@Request() request, @Body() requestBody) {
    await this.authenticationService.resetpassword(
      request.user.sub,
      requestBody,
    );
  }

  @Post('/login')
  async login(@Body() requestBody: LoginDto) {
    return this.authenticationService.login(requestBody);
  }
}
