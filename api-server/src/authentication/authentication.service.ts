import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/user/entities/user.entity';
import * as bcryptjs from 'bcryptjs';
import { LoginDto } from './authentication.dto';

@Injectable()
export class AuthenticationService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  async start({ username }: { username: string }) {
    const userExists = await this.userModel.findOne({ username });
    if (userExists) {
      throw new BadRequestException('User already exists');
    }

    const user = await this.userModel.create({ username, name: username });
    const jwtTokens = await this.generateNewJwtTokens(user.id, username);
    return this.generateJwtPayload(
      jwtTokens.accessToken,
      jwtTokens.refreshToken,
    );
  }

  async generateJwtPayload(accessToken: string, refreshToken: string) {
    const expiresIn = this.configService.get('JWT_ACCESS_EXPIRES');
    return {
      accessToken,
      refreshToken,
      expiresIn,
      expiresAt: (new Date().getTime() + expiresIn * 1000).toString(),
    };
  }

  async generateNewJwtTokens(userId: string, username: string) {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        {
          username,
          sub: userId,
        },
        {
          secret: this.configService.get('JWT_ACCESS_SECRET'),
          expiresIn: `${this.configService.get('JWT_ACCESS_EXPIRES')}s`,
        },
      ),
      this.jwtService.signAsync(
        {
          username,
          sub: userId,
        },
        {
          secret: this.configService.get('JWT_REFRESH_SECRET'),
          expiresIn: `${this.configService.get('JWT_REFRESH_EXPIRES')}s`,
        },
      ),
    ]);

    return { accessToken, refreshToken };
  }

  async refreshJwtTokens(userId: string) {
    const user = await this.userModel.findById(userId);
    const jwtTokens = await this.generateNewJwtTokens(userId, user.username);
    return this.generateJwtPayload(
      jwtTokens.accessToken,
      jwtTokens.refreshToken,
    );
  }

  async getCurrentUser(userId: string) {
    return this.userModel.findById(userId);
  }

  async resetpassword(userId: string, requestBody: { password: string }) {
    const hashPassword = await bcryptjs.hash(requestBody.password, 10);
    await this.userModel.findByIdAndUpdate(userId, {
      password: hashPassword,
    });
  }

  async login({ username, password }: LoginDto) {
    const user: any = await this.userModel
      .findOne(
        { username },
        { _id: 1, name: 1, username: 1, avatarUrl: 1, password: 1, id: true },
      )
      .lean();
    if (!user) {
      throw new NotFoundException('Tên người dùng không chính xác');
    }

    const isPasswordMatching = await bcryptjs.compare(password, user.password);
    if (!isPasswordMatching) {
      throw new BadRequestException('Mật khẩu không chính xác');
    }
    const jwtTokens = await this.generateNewJwtTokens(user._id, user.username);
    const tokens = await this.generateJwtPayload(
      jwtTokens.accessToken,
      jwtTokens.refreshToken,
    );

    return {
      ...tokens,
      currentUser: user,
    };
  }
}
