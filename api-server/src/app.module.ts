import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { MessageModule } from './message/message.module';
import { ConversationModule } from './conversation/conversation.module';
import { ParticipantModule } from './participant/participant.module';
import { ContactModule } from './contact/contact.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DeviceModule } from './device/device.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { FileModule } from './file/file.module';
import { AddFriendRequestModule } from './add-friend-request/add-friend-request.module';
import { ChatModule } from './chat/chat.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get('MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    UserModule,
    MessageModule,
    ConversationModule,
    ParticipantModule,
    ContactModule,
    ConfigModule.forRoot(),
    DeviceModule,
    AuthenticationModule,
    FileModule,
    AddFriendRequestModule,
    ChatModule,
  ],
})
export class AppModule {}
