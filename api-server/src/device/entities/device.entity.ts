import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type DeviceDocument = HydratedDocument<Device>;

@Schema({ timestamps: true })
export class Device {
  @Prop({
    required: true,
  })
  deviceToken: string;

  @Prop()
  type: string;

  static TYPE = {
    WEB: 'web',
    ANDROID: 'android',
    IOS: 'ios',
  };
}

export const DeviceFactory: ModelDefinition = {
  name: Device.name,
  schema: SchemaFactory.createForClass(Device),
};
