import { Module } from '@nestjs/common';
import { AddFriendRequestService } from './add-friend-request.service';
import { AddFriendRequestController } from './add-friend-request.controller';

@Module({
  controllers: [AddFriendRequestController],
  providers: [AddFriendRequestService],
})
export class AddFriendRequestModule {}
