import { Model } from 'mongoose';
import { BaseEntity } from './base.entity';
import {
  Post,
  Body,
  Get,
  Param,
  Delete,
  Put,
  UseGuards,
  Query,
  Request,
} from '@nestjs/common';
import { AccessTokenGuard } from './authentication/guards/accessToken.guard';
import { omit } from 'lodash';
import { Utils } from 'utils';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;

export class BaseController<S extends BaseEntity> {
  constructor(
    private readonly baseModel: Model<S>,
    private searchFields?: string[],
  ) {}

  @UseGuards(AccessTokenGuard)
  @Post()
  create(@Body() requestCreate) {
    return this.baseModel.create(requestCreate);
  }

  @UseGuards(AccessTokenGuard)
  @Get()
  findAll(@Query() query) {
    const { filter, limit, skip } = query;
    return this.baseModel.find(filter, null, { skip, limit });
  }

  @UseGuards(AccessTokenGuard)
  @Get('search')
  search(@Query() query, @Request() request) {
    const currentUserId = request.user.sub;
    const { filter: filterJson = '{}', skip = 0, limit = 10 } = query || {};

    const filter = Utils.jsonValid(filterJson) ? JSON.parse(filterJson) : {};
    const _q = filter?._q;
    const normalizeFilter = omit(filter, '_q');

    if (!this.searchFields.length) {
      return this.baseModel.find(normalizeFilter, null, { skip, limit });
    }

    return this.baseModel
      .find(
        {
          ...normalizeFilter,
          _id: { $ne: new ObjectId(currentUserId) },
          $text: { $search: _q },
        },
        { score: { $meta: 'textScore' } },
        { skip, limit },
      )
      .sort({ score: { $meta: 'textScore' } });
  }

  @UseGuards(AccessTokenGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.baseModel.findById(id);
  }

  @UseGuards(AccessTokenGuard)
  @Put(':id')
  async update(@Param('id') id: string, @Body() requestUpdate) {
    try {
      await this.baseModel.updateOne({ _id: id }, requestUpdate);
      return this.baseModel.findById(id);
    } catch (error) {
      console.log(error);
    }
  }

  @UseGuards(AccessTokenGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.baseModel.deleteOne({ id });
  }
}
