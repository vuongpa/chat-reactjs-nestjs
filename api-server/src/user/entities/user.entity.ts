import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { Device } from 'src/device/entities/device.entity';

export type UserDocument = HydratedDocument<User>;

@Schema({
  timestamps: true,
})
export class User {
  @Prop({ required: true, unique: true, index: true })
  username: string;

  @Prop({
    type: [mongoose.Schema.Types.ObjectId],
    ref: Device.name,
  })
  devices: Device[];

  @Prop()
  password: string;

  @Prop()
  name: string;

  @Prop()
  avatarUrl: string;
}

export const UserFactory: ModelDefinition = {
  name: User.name,
  schema: SchemaFactory.createForClass(User).index({
    username: 'text',
    name: 'text',
  }),
};
