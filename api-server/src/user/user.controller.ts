import { Get, UseGuards, Query, Request, Controller } from '@nestjs/common';
import { BaseController } from 'src/base.controller';
import { User } from './entities/user.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { omit } from 'lodash';
import { AccessTokenGuard } from 'src/authentication/guards/accessToken.guard';
import { Utils } from 'utils';
import { Message } from 'src/message/entities/message.entity';
import { MessageService } from 'src/message/message.service';
import { UserService } from './user.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;

@Controller('users')
export class UserController extends BaseController<User> {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    @InjectModel(Message.name)
    private readonly messageModel: Model<Message>,
    private readonly messageService: MessageService,
    private readonly userService: UserService,
  ) {
    super(userModel, ['name', 'username']);
  }

  @UseGuards(AccessTokenGuard)
  @Get('search')
  async searchUser(@Query() query, @Request() request) {
    const currentUserId = request.user.sub;
    const { filter: filterJson = '{}', skip = 0, limit = 10 } = query || {};

    const filter = Utils.jsonValid(filterJson) ? JSON.parse(filterJson) : {};
    const _q = filter?._q;
    const normalizeFilter = omit(filter, '_q');

    const result = await this.userModel
      .find(
        {
          ...normalizeFilter,
          _id: { $ne: new ObjectId(currentUserId) },
          $text: { $search: _q },
        },
        { score: { $meta: 'textScore' } },
        { skip, limit, sort: { score: { $meta: 'textScore' } } },
      )
      .lean();

    const lastMessages = await Promise.all(
      result.map((user) => {
        return this.messageService.findLastMessage(currentUserId, user._id);
      }),
    );

    return result.map((user) => ({
      ...user,
      lastMessage: lastMessages.find(
        (message) =>
          (message?.sender?.id == currentUserId &&
            message?.receiver?.id == user._id) ||
          (message?.sender?.id == user._id &&
            message?.receiver?.id == currentUserId),
      ),
    }));
  }

  @UseGuards(AccessTokenGuard)
  @Get('conversation')
  async getConversation(@Request() request, @Query() query) {
    const { limit = 10 } = query;
    const currentUserId = request.user.sub;
    return this.userService.lastMessage(currentUserId, limit);
  }
}
