/* eslint-disable @typescript-eslint/no-var-requires */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './entities/user.entity';
import { HandleDeviceDto } from './user.dto';
import { Message } from 'src/message/entities/message.entity';

const ObjectId = require('mongodb').ObjectId;

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    @InjectModel(Message.name)
    private readonly messageModel: Model<Message>,
  ) {}

  async addDevice({ userId, deviceId }: HandleDeviceDto): Promise<User> {
    return this.userModel.findByIdAndUpdate(userId, {
      $push: {
        devices: deviceId,
      },
    });
  }

  async removeDevice({ userId, deviceId }: HandleDeviceDto): Promise<User> {
    return this.userModel.findByIdAndUpdate(userId, {
      $pull: {
        devices: deviceId,
      },
    });
  }

  async lastMessage(currentUserId: any, limit = 10) {
    const result = await this.messageModel.aggregate([
      {
        $match: {
          $or: [
            { sender: new ObjectId(currentUserId) },
            { receiver: new ObjectId(currentUserId) },
          ],
        },
      },
      { $sort: { messageId: -1 } },
      {
        $addFields: {
          senderId: { $toString: '$sender' },
          receiverId: { $toString: '$receiver' },
        },
      },
      {
        $group: {
          _id: '$conversationId',
          doc: { $first: '$$ROOT' },
        },
      },
      { $limit: limit },
      { $replaceRoot: { newRoot: '$doc' } },
    ]);

    return this.messageModel.populate(result, [
      { path: 'receiver' },
      { path: 'sender' },
    ]);
  }
}
