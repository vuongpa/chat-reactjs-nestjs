import { IsString } from 'class-validator';

export class HandleDeviceDto {
  @IsString()
  userId: string;

  @IsString()
  deviceId: string;
}
