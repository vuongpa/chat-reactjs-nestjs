import { LoginPayload } from '@/app/auth/slice';
import { formUtils } from '@/utils/form-utils';
import { IAuthPayload } from '@/utils/interfaces';
import AxiosRequest from '@/utils/request-utils';

const authApi = {
  login(params: LoginPayload): Promise<IAuthPayload> {
    return AxiosRequest.post(formUtils.endpointWithQuery('/auth/login'), params);
  },
};
export default authApi;
