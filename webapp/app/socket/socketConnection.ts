import Cookies from 'js-cookie';
import { Socket, io } from 'socket.io-client';

import { AUTH_KEY } from '@/utils/constants';

export default function createWebSocketConnection(): Socket {
  const url = process.env.NEXT_PUBLIC_WEBSOCKET || '';
  const accessToken = Cookies.get(AUTH_KEY.ACCESS_TOKEN) || '';

  return io(url, {
    extraHeaders: {
      Authorization: accessToken,
    },
  });
}
