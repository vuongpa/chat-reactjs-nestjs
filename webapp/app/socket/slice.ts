import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { AppState } from '../store';

import { Conversation, Message } from '@/models/message';

export interface SendMessageError {
  error: string;
  conversationId: string;
}

export interface SocketState {
  isConnected: boolean;
  conversationList: Conversation[];
  selectedConversation?: Conversation;
  error: string;
  loading?: boolean;
}

const initialState: SocketState = {
  isConnected: false,
  conversationList: [],
  selectedConversation: undefined,
  loading: false,
  error: '',
};

export const socketSlice = createSlice({
  name: 'socket',
  initialState,
  reducers: {
    connect: (state: SocketState) => {
      state.isConnected = true;
    },
    disconnect: (state: SocketState) => {
      state.isConnected = false;
    },
    requestConversationList(state, action: PayloadAction<Conversation[]>) {
      state.loading = true;
    },
    requestConversationListSuccess(state, action: PayloadAction<Conversation[]>) {
      state.loading = false;
      state.conversationList = [
        ...state.conversationList,
        ...action.payload,
      ];
    },
    requestConversationListFail(state, action: PayloadAction<string>) {
      state.loading = false;
      state.error = action.payload;
    },
    requestMessage(state, action: PayloadAction<Message>) {
      state.loading = true;
      const message = action.payload;
      state.conversationList = state.conversationList.map((conversation: Conversation) => {
        conversation.active = conversation._id === message.conversationId;
        return conversation;
      });
    },
    requestMessageSuccess(state, action: PayloadAction<Message[]>) {
      state.loading = false;
      const messages = action.payload;

      state.conversationList = state.conversationList
        .map((conversation: Conversation) => {
          if (conversation.active) {
            conversation.messages = [...conversation.messages, ...messages];
            conversation.sending = false;
          }

          return conversation;
        });
    },
    requestMessageFail(state, action: PayloadAction<string>) {
      state.loading = false;
      state.error = action.payload;
    },
    sendMessage(state, action: PayloadAction<Message>) {
      const message = action.payload;
      const { conversationList } = state;

      let conversationIndex = -1;
      const conversation = conversationList.find((item: Conversation, index: number) => {
        if (item._id === message.conversationId) {
          conversationIndex = index;
          return true;
        }
        return false;
      });

      const newMessage: Message = {
        sender: {},
        receiver: {},
        text: '',
      };

      if (conversationIndex === -1) {
        const newConversation: Conversation = {
          messages: [newMessage],
          lastMessage: newMessage,
          active: false,
          sending: true,
          error: '',
        };
        state.conversationList = [...conversationList, newConversation];
      }

      if (conversationIndex !== -1 && conversation) {
        state.conversationList[conversationIndex] = { ...conversation, messages: [...conversation.messages, newMessage] };
      }
    },
    sendMessageSuccess(state, action: PayloadAction<Message>) {
      const message = action.payload;
      state.conversationList.map((conversation: Conversation) => {
        if (conversation._id === message.conversationId) {
          const { messages } = conversation;
          messages[messages.length - 1] = message;

          return {
            ...conversation,
            lastMessage: message,
            messages,
            sending: false,
            error: '',
            active: true,
          };
        }

        return conversation;
      });
    },
    sendMessageFail(state, action: PayloadAction<SendMessageError>) {
      state.conversationList.map((conversation: Conversation) => {
        if (conversation._id === action.payload.conversationId) {
          return {
            ...conversation,
            sending: false,
            error: action.payload.error,
            active: true,
          };
        }

        return conversation;
      });
    },
  },
});

export const socketActions = socketSlice.actions;

export const selectConversationList = (state: AppState) => state.socket.conversationList;
export const selectSelectedConversation = (state: AppState) => state.socket.selectedConversation;
export const selectChatError = (state: AppState) => state.socket.error;
export const selectIsSocketConnected = (state: AppState) => state.socket.isConnected;
