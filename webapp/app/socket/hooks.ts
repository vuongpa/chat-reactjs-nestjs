import { useDispatch, useSelector } from 'react-redux';

import { AppState } from '../store';

import { socketActions } from './slice';

export function useIsConnected() {
  return useSelector((state: AppState) => state.socket.isConnected);
}

export function useSocketConnect() {
  const dispatch = useDispatch();
  return () => {
    dispatch(socketActions.connect());
  };
}

export function useSocketDisconnect() {
  const dispatch = useDispatch();
  return () => {
    dispatch(socketActions.disconnect());
  };
}

export function useSocket() {
  return useSelector((state: AppState) => state.socket.socketInstance);
}

export function useSetSocket() {
  const dispatch = useDispatch();
  return (socketInstance: any) => {
    dispatch(socketActions.setSocket(socketInstance));
  };
}
