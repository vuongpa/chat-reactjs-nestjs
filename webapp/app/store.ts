import { configureStore } from '@reduxjs/toolkit';
import { createWrapper } from 'next-redux-wrapper';
import createSagaMiddleware from 'redux-saga';
import { createRouterMiddleware } from 'connected-next-router';

import rootSaga from './saga';
import rootReducer from './rootReducer';

const sagaMiddleware = createSagaMiddleware();
const routerMiddleware = createRouterMiddleware();

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => [...getDefaultMiddleware(), sagaMiddleware, routerMiddleware],
  devTools: true,
});

sagaMiddleware.run(rootSaga);

const makeStore = () => store;

export type AppStore = ReturnType<typeof makeStore>;
export type AppState = ReturnType<AppStore['getState']>;
export const wrapper = createWrapper<AppStore>(makeStore);
