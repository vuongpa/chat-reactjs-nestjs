import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { AppState } from '../store';

import { User } from '@/models/user';
import { AuthUtils } from '@/utils/auth.utils';
import { IAuthPayload } from '@/utils/interfaces';

export interface LoginPayload {
  username: string;
  password: string;
}

export interface AuthState {
  isLoggedIn: boolean;
  logging: boolean;
  currentUser?: User;
  error: string;
}

const initialState: AuthState = {
  isLoggedIn: false,
  logging: false,
  currentUser: undefined,
  error: '',
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setCurrentUser(state, action) {
      state.currentUser = action.payload;
      return state;
    },

    login(state, action: PayloadAction<LoginPayload>) {
      state.logging = true;
    },

    loginSuccess(state, action: PayloadAction<IAuthPayload>) {
      state.logging = false;
      state.isLoggedIn = true;
      state.error = '';

      const { currentUser, ...tokens } = action.payload;
      state.currentUser = currentUser;
      AuthUtils.setAuthToken(tokens);
    },

    loginFail(state, action: PayloadAction<string>) {
      state.logging = false;
      state.isLoggedIn = false;
      state.error = action.payload;
      AuthUtils.removeAllToken();
    },

    logout(state) {
      state.isLoggedIn = false;
      state.currentUser = undefined;
      AuthUtils.removeAllToken();
    },
  },
});

export const authActions = authSlice.actions;

export const selectIsLoggedIn = (state: AppState) => state.auth.isLoggedIn;
export const selectIsLogging = (state: AppState) => state.auth.logging;
export const selectLoginError = (state: AppState) => state.auth.error;
export const selectCurrentUser = (state: AppState) => state.auth.currentUser;
