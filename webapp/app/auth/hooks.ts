import { useDispatch, useSelector } from 'react-redux';

import { authActions, selectCurrentUser } from './slice';

import { User } from '@/models/user';

export function useCurrentUser() {
  return useSelector(selectCurrentUser);
}

export function useCurrentUserId() {
  const currentUser = useCurrentUser();
  return currentUser?._id;
}

export function useSetCurrentUser() {
  const dispatch = useDispatch();
  return (user: User) => {
    dispatch(authActions.setCurrentUser(user));
  };
}
