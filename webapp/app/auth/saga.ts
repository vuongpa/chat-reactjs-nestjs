import { PayloadAction, createAction } from '@reduxjs/toolkit';
import {
  call, put, takeEvery, takeLatest,
} from 'redux-saga/effects';
import { push } from 'connected-next-router';

import { LoginPayload, authActions } from './slice';

import authApi from '@/api/authApi';
import AxiosRequest from '@/utils/request-utils';
import { formUtils } from '@/utils/form-utils';

export const fetchMeAction = createAction('auth/fetchMe');

export type GeneratorType = Generator<any, any, any>

function* fetchMe(): GeneratorType {
  try {
    const url = formUtils.endpointWithQuery('/auth/me');
    const currentUser = yield call(AxiosRequest.get, url);
    yield put(authActions.setCurrentUser(currentUser));
  } catch (err: any) {
    console.log('error fetch me: ', err.message);
  }
}

function* handleLogin(action: PayloadAction<LoginPayload>): GeneratorType {
  try {
    const loginResponse = yield call(authApi.login, action.payload);
    yield put(authActions.loginSuccess(loginResponse));
    yield put(push('/'));
  } catch (error: any) {
    yield put(authActions.loginFail(error.message));
  }
}

function* handleLogout() {
  yield put(push('/login'));
}

export default function* authSaga() {
  yield takeLatest(authActions.logout, handleLogout);
  yield takeEvery(authActions.login, handleLogin);
  yield takeEvery(fetchMeAction, fetchMe);
}
