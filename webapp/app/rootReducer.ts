import { combineReducers } from '@reduxjs/toolkit';
import { routerReducer } from 'connected-next-router';

import { authSlice } from './auth/slice';
import { menuSlice } from './menu/slice';
import { socketSlice } from './socket/slice';

import { searchConversationSlice } from '@/components/features/message/search-conversation/redux/slice';
import { conversationSlice } from '@/components/features/message/conversation/redux/slice';
import { addContactSlice } from '@/components/features/contact/add-contact/redux/slice';

const rootReducer = combineReducers({
  router: routerReducer,
  [authSlice.name]: authSlice.reducer,
  [menuSlice.name]: menuSlice.reducer,
  [searchConversationSlice.name]: searchConversationSlice.reducer,
  [conversationSlice.name]: conversationSlice.reducer,
  [addContactSlice.name]: addContactSlice.reducer,
  [socketSlice.name]: socketSlice.reducer,
});

export default rootReducer;
