import { useSelector } from 'react-redux';

import { AppState } from '../store';

export function useActiveKeyOfMenu() {
  return useSelector((state: AppState) => state.menu.activeKey);
}
