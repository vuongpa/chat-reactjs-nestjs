import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

export interface IMenuState {
  activeKey: string;
}

const initialState: IMenuState = {
  activeKey: 'message',
};

export const menuSlice = createSlice({
  name: 'menu',
  initialState,
  reducers: {
    setActiveKey(state, action) {
      state.activeKey = action.payload;
    },
  },

  extraReducers: {
    [HYDRATE]: (state, action) => ({
      ...state,
      ...action.payload.auth,
    }),
  },
});

export const { setActiveKey } = menuSlice.actions;
export default menuSlice.reducer;
