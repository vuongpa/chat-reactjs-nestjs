import { all } from 'redux-saga/effects';

import authSaga from './auth/saga';
import socketSaga from './socket/socketSaga';

import conversationSaga from '@/components/features/message/conversation/redux/sagas';

export default function* rootSaga() {
  yield all([
    authSaga(),
    conversationSaga(),
    socketSaga(),
  ]);
}
