import {
  Form, Input, Button, Typography,
} from 'antd';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
import { useCallback } from 'react';

import styles from '@/styles/Login.module.scss';
import { authActions } from '@/app/auth/slice';

export default function LoginPage() {
  const dispatch = useDispatch();

  const onFinish = useCallback((formValue: any) => {
    dispatch(authActions.login(formValue));
  }, []);

  return (
    <div className={styles.login}>
      <Typography.Title className={styles.title} level={2}>Chat App</Typography.Title>
      <Form
        name="updateUserInfo"
        onFinish={onFinish}
        scrollToFirstError
        layout="vertical"
        style={{ maxWidth: 350 }}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập username',
            },
          ]}
        >
          <Input size="large" placeholder="Username" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập mật khẩu!',
            },
            {
              min: 6,
              message: 'Mật khẩu phải có ít nhất 6 kí tự',
            },
          ]}
          hasFeedback
        >
          <Input.Password size="large" placeholder="Mật khẩu" />
        </Form.Item>
        <Form.Item>
          <Button style={{ width: 350 }} size="large" type="primary" htmlType="submit">
            Đăng nhập
          </Button>
        </Form.Item>
        <div className={styles.start}>
          <Link href="/start">
            Tạo mới tại đây -
            {'>'}
          </Link>
        </div>
      </Form>
    </div>
  );
}
