import React from 'react';
import {
  ContactsOutlined,
  MessageOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import {
  Button, Layout, Menu, Typography,
} from 'antd';
import { useDispatch } from 'react-redux';
import dynamic from 'next/dynamic';
import { map, omit } from 'lodash';

import styles from '@/Styles/Home.module.scss';
import AvatarWithOption from '@/components/features/avatar-option/AvatarOption';
import { useCurrentUser } from '@/app/auth/hooks';
import { useActiveKeyOfMenu } from '@/app/menu/hooks';
import { setActiveKey } from '@/app/menu/slice';
import { fetchMeAction } from '@/app/auth/saga';

const { Header, Content, Sider } = Layout;

const Message = dynamic(() => import('@/components/features/message/Message'), { ssr: false });
const Contact = dynamic(() => import('@/components/features/contact/Contact'), { ssr: false });
const Setting = dynamic(() => import('@/components/features/setting/Setting'), { ssr: false });

const menuItems = [
  { key: 'message', icon: <MessageOutlined />, Component: <Message /> },
  { key: 'contact', icon: <ContactsOutlined />, Component: <Contact /> },
  { key: 'setting', icon: <SettingOutlined />, Component: <Setting /> },
];

export default function Home() {
  const currentUser = useCurrentUser();
  const dispatch = useDispatch();
  const activeKey = useActiveKeyOfMenu();

  const renderFeature = () => {
    switch (activeKey) {
      case 'message': {
        return menuItems.find((el) => el.key === 'message')?.Component;
      }
      case 'contact': {
        return menuItems.find((el) => el.key === 'contact')?.Component;
      }
      case 'setting': {
        return menuItems.find((el) => el.key === 'setting')?.Component;
      }
      default: return null;
    }
  };

  return (
    <Layout className={styles['main-layout']}>
      <Sider width={64}>
        <div className={styles.logo}>
          <AvatarWithOption />
        </div>
        <Menu
          theme="dark"
          defaultActiveFirst
          activeKey={activeKey}
          defaultSelectedKeys={[activeKey]}
          onClick={(e) => {
            dispatch(setActiveKey(e.key));
          }}
          mode="inline"
          items={map(menuItems, (menuItem) => omit(menuItem, 'Component'))}
        />
      </Sider>
      <Layout className={styles['site-layout']}>
        <Header className={styles.header}>
          <Typography>{currentUser?.name}</Typography>
        </Header>
        <Content>
          <div className={styles['main-content']}>
            {renderFeature()}
          </div>
        </Content>
      </Layout>
    </Layout>
  );
}
