import type { AppProps } from 'next/app';
import * as React from 'react';
import { Provider, useDispatch } from 'react-redux';

import { store, wrapper } from '@/app/store';
import { AuthUtils } from '@/utils/auth.utils';
import { fetchMeAction } from '@/app/auth/saga';

import '@/styles/globals.css';

function App({ Component, pageProps }: AppProps) {
  const dispatch = useDispatch();

  React.useEffect(() => {
    const { accessToken } = AuthUtils.getAuthToken();
    if (!accessToken) {
      return;
    }

    dispatch(fetchMeAction());
  }, []);

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default wrapper.withRedux(App);
