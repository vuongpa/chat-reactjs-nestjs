import { AnyObject } from '@/utils/type';

export interface Message {
  _id?: string;
  conversationId?: string;
  sender: AnyObject | string;
  receiver: AnyObject | string;
  text: string;
  messageId?: number;
  conversation?: AnyObject | string;
}

export interface Conversation {
  _id?: string;
  messages: Message[];
  lastMessage: Message;
  active: boolean;
  sending: boolean;
  error: string;
}
