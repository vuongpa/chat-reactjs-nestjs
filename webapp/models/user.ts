export interface User {
  _id: string;
  name: string;
  username: string;
  devices?: string[];
  password?: string;
  avatarUrl?: string;
}
