import { FC, ReactNode, memo } from 'react';
import cls from 'classnames';

import styles from './Container.module.css';

interface ContainerProps {
  size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
  children: ReactNode;
}

const Container: FC<ContainerProps> = memo(({
  size = 'md',
  children,
}) => (
  <div className={cls(styles.container, {
    [styles.xsContainer]: size === 'xs',
    [styles.smContainer]: size === 'sm',
    [styles.mdContainer]: size === 'md',
    [styles.lgContainer]: size === 'lg',
    [styles.xlContainer]: size === 'xl',
  })}
  >
    {children}
  </div>
));

export default Container;
