import React, { useEffect, useState } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import { Upload } from 'antd';
import type { UploadProps } from 'antd/es/upload';
import type { UploadFile } from 'antd/es/upload/interface';
import Cookies from 'js-cookie';

import styles from './Upload.module.scss';

import { AUTH_KEY } from '@/utils/constants';

const uploadButton = (
  <div>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>Upload</div>
  </div>
);

interface UploadImageProp extends UploadProps {
  value?: any;
  onChange?: (value?: any) => void;
}

export default function UploadImage({ value, onChange, ...restProps }: UploadImageProp) {
  const [fileList, setFileList] = useState<UploadFile[]>([]);

  const onListChange = (info: any) => {
    setFileList(info.fileList);
    onChange?.(info.fileList?.[0]?.response?.url);
  };

  useEffect(() => {
    if (value) {
      setFileList([{ url: value, uid: Math.random().toString(), name: value }]);
    }
  }, []);

  return (
    <Upload
      {...restProps}
      action={`${process.env.NEXT_PUBLIC_API_DOMAIN}/files/upload`}
      headers={{ Authorization: `Bearer ${Cookies.get(AUTH_KEY.ACCESS_TOKEN)}` }}
      fileList={fileList}
      onChange={onListChange}
      multiple={false}
      listType="picture-circle"
      showUploadList
    >
      {!fileList.length && uploadButton}
    </Upload>
  );
}
