import { Form, Popover } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import { SmileOutlined, FileImageOutlined, LinkOutlined } from '@ant-design/icons';
import EmojiPicker, { EmojiClickData } from 'emoji-picker-react';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';

import OpacityClick from '../../opacity-click/OpacityClick';

import styles from './InputChat.module.scss';

import { useUserIdInChat } from '@/components/features/message/conversation/redux/hooks';
import { useSocket } from '@/app/socket/hooks';
import { fetchConversations, fetchMessagesInChat } from '@/components/features/message/conversation/redux/slice';
import { useCurrentUser } from '@/app/auth/hooks';
import { socketActions } from '@/app/socket/slice';

const { useForm, Item } = Form;

export default function InputChat() {
  const [typing, setTyping] = useState<boolean>(false);
  const [form] = useForm();
  const inputRef = useRef<HTMLTextAreaElement>(null);
  const userIdInChat = useUserIdInChat();
  const currentUser = useCurrentUser();
  const dispatch = useDispatch();

  const handleTyping = () => {
    const text = form.getFieldValue('text');
    if (text && !typing) {
      setTyping(true);
    }

    if (!text && typing) {
      setTyping(false);
    }
  };

  const focusInput = () => inputRef.current?.focus();

  const focusInputAfterSendMessage = () => {
    form.setFieldValue('text', '');
    focusInput();
  };

  const onEmojiClick = (emojiData: EmojiClickData) => {
    const text = form.getFieldValue('text') || '';
    form.setFieldValue('text', `${text} ${emojiData.emoji}`);
    focusInput();
  };

  const handleSendMessage = () => {
    const text = form.getFieldValue('text');

    dispatch(socketActions.sendMessage({
      text,
      sender: currentUser?._id || '',
      receiver: userIdInChat,
    }));

    focusInputAfterSendMessage();
  };

  const handleInputKeyDown = (e: React.KeyboardEvent) => {
    if (e.code !== 'Enter') {
      return;
    }

    e.preventDefault();
    handleSendMessage();
  };

  useEffect(() => focusInput(), []);

  return (
    <div className={styles['input-chat']}>
      <div className={classNames(styles.header)}>
        <Popover
          trigger="click"
          arrow={false}
          content={(
            <EmojiPicker
              onEmojiClick={onEmojiClick}
              lazyLoadEmojis
              autoFocusSearch
              searchPlaceHolder="Tìm kiếm"
            />
        )}
        >
          <OpacityClick>
            <SmileOutlined style={{ fontSize: 20 }} />
          </OpacityClick>
        </Popover>
        <OpacityClick>
          <FileImageOutlined style={{ fontSize: 20 }} />
        </OpacityClick>
        <OpacityClick>
          <LinkOutlined style={{ fontSize: 20 }} />
        </OpacityClick>
      </div>
      <div className={styles.input}>
        <Form form={form} name="input-chat">
          <Item name="text">
            <TextareaAutosize
              className={styles['text-area-autosize']}
              onChange={handleTyping}
              rows={1}
              ref={inputRef}
              placeholder="@ Nhập tin nhắn"
              onKeyDown={handleInputKeyDown}
            />
          </Item>
        </Form>
        <div className={styles.send}>
          <OpacityClick onClick={handleSendMessage}>
            GỬI
          </OpacityClick>
        </div>
      </div>
    </div>
  );
}
