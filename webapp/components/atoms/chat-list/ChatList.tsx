import classNames from 'classnames';
import { useEffect, useRef } from 'react';

import ChatItem, { ChatItemProps } from '../chat-item/ChatItem';

import styles from './ChatList.module.scss';

interface ChatListProps {
  className?: string;
  dataSource?: ChatItemProps[];
}

export default function ChatList(props: ChatListProps) {
  const { className, dataSource = [] } = props;
  const messagesEndRef = useRef<any>(null);

  const scrollToLastMessage = () => {
    messagesEndRef.current?.scrollIntoView();
  };

  useEffect(() => {
    scrollToLastMessage();
  }, [dataSource]);

  return (
    <div className={classNames(styles['chat-list'], className)}>
      {dataSource.map((chatItemProps: ChatItemProps, index: number) => {
        if (dataSource.length - 1 === index) {
          return (
            <div key={chatItemProps.id}>
              <ChatItem {...chatItemProps} />
              <div ref={messagesEndRef} />
            </div>
          );
        }

        return (
          <ChatItem key={chatItemProps.id} {...chatItemProps} />
        );
      })}
    </div>
  );
}
