import { LikeOutlined } from '@ant-design/icons';
import classNames from 'classnames';
import React from 'react';

import styles from './LikeChat.module.scss';

interface LikeProps extends Omit<React.HTMLAttributes<HTMLDivElement>, 'ref'> {
  className?: string;
}

export default function LikeChat(props: LikeProps) {
  const { className, ...restProps } = props;

  return (
    <div
      className={classNames(styles['like-chat'], className)}
      {...restProps}
    >
      <LikeOutlined />
    </div>
  );
}
