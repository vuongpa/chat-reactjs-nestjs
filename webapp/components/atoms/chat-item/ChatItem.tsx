import classNames from 'classnames';
import { Avatar, Typography } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import moment from 'moment';
import { useEffect, useState } from 'react';

import styles from './ChatItem.module.scss';
import LikeChat from './like/LikeChat';
import RemoveChat from './remove/RemoveChat';
import ReplyChat from './reply/ReplyChat';

export interface ChatItemProps {
  id?: string;
  className?: string;
  type?: 'text' | 'file' | 'call';
  position?: 'left' | 'right';
  removeAction?: boolean;
  replyAction?: boolean;
  likeAction?: boolean;
  avatarUrl?: string;
  time: string;
  status?: 'read' | 'unread' | 'error';
  message?: any;
  showAvatar?: boolean;
}

export default function ChatItem(props: ChatItemProps) {
  const {
    id,
    className,
    type = 'text',
    position,
    removeAction = true,
    replyAction = true,
    likeAction = true,
    avatarUrl,
    time,
    status,
    showAvatar,
    message,
  } = props;

  const [mouseEnter, setMouseEnter] = useState<boolean>(false);
  const isLeft = position === 'left';
  const isRight = position === 'right';

  const renderTime = (t: string) => {
    const timeMoment = moment(t);
    const isToday = timeMoment.day() === moment().day();
    if (isToday) {
      return timeMoment.format('hh:mm');
    }

    const isYear = timeMoment.year() === moment().year();
    if (isYear) {
      return timeMoment.format('hh:mm DD/MM');
    }

    return timeMoment.format('hh:mm DD/MM/YYYY');
  };

  useEffect(() => {
    setMouseEnter(false);
  }, []);

  return (
    <div
      className={classNames(
        styles['chat-item'],
        className,
        {
          [styles['position-left']]: isLeft,
          [styles['position-right']]: isRight,
        },
      )}
      onMouseEnter={() => setMouseEnter(true)}
      onMouseLeave={() => setMouseEnter(false)}
    >
      <div className={classNames(styles.message, {
        [styles['message-left']]: isLeft,
      })}
      >
        <div className={classNames(styles.action, {
          [styles['action-with-message-right']]: isRight,
        })}
        >
          {mouseEnter && (
            <>
              <RemoveChat />
              <ReplyChat />
            </>
          )}
        </div>
        <div className={styles.text}>
          {message}
          <Typography.Paragraph className={styles.time} type="secondary">
            {renderTime(time)}
          </Typography.Paragraph>
          {isLeft && mouseEnter && <LikeChat />}
        </div>
        {showAvatar ? (
          <Avatar
            size="large"
            icon={<UserOutlined />}
            src={avatarUrl}
            className={styles.avatar}
          />
        ) : <div className={styles.avatarBox} />}
      </div>
    </div>
  );
}
