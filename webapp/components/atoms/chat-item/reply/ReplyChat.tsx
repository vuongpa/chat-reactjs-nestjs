import classNames from 'classnames';
import React from 'react';
import { EnterOutlined } from '@ant-design/icons';

import OpacityClick from '../../opacity-click/OpacityClick';

import styles from './ReplyChat.module.scss';

interface ReplyChatProps extends Omit<React.HTMLAttributes<HTMLDivElement>, 'ref'> {
  className?: string;
}

export default function ReplyChat(props: ReplyChatProps) {
  const { className, ...restProps } = props;

  return (
    <div
      className={classNames(styles['reply-chat'], className)}
      {...restProps}
    >
      <OpacityClick>
        <EnterOutlined />
      </OpacityClick>
    </div>
  );
}
