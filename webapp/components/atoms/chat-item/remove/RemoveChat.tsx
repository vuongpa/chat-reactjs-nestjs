import classNames from 'classnames';
import React from 'react';
import { DeleteOutlined } from '@ant-design/icons';

import OpacityClick from '../../opacity-click/OpacityClick';

import styles from './RemoveChat.module.scss';

interface RemoveChatProps extends Omit<React.HTMLAttributes<HTMLDivElement>, 'ref'> {
  className?: string;
}

export default function RemoveChat(props: RemoveChatProps) {
  const { className, ...restProps } = props;

  return (
    <div
      className={classNames(styles['remove-chat'], className)}
      {...restProps}
    >
      <OpacityClick>
        <DeleteOutlined />
      </OpacityClick>
    </div>
  );
}
