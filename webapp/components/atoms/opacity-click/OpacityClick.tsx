import classNames from 'classnames';

import styles from './OpacityClick.module.scss';

export default function OpacityClick(props: Omit<React.HTMLAttributes<HTMLDivElement>, 'ref'>) {
  const { className, children, ...restProps } = props;
  return (
    <div
      className={classNames(styles.opacity, className)}
      {...restProps}
    >
      {children}
    </div>
  );
}
