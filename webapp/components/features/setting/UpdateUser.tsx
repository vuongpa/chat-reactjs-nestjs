import { Form, Input, Button } from 'antd';

import UploadImage from '@/components/atoms/upload/Upload';
import { formUtils } from '@/utils/form-utils';
import { useSetCurrentUser, useCurrentUser } from '@/app/auth/hooks';

export default function UpdateUser() {
  const setCurrentUser = useSetCurrentUser();
  const currentUser = useCurrentUser();

  const onFinish = (values: any) => {
    formUtils.submitForm({
      method: 'PUT',
      endpoint: `/users/${currentUser?._id}`,
      notify: true,
      onGotSuccess: (updatedUser) => {
        setCurrentUser(updatedUser);
      },
    }, values);
  };

  return (
    <Form
      name="updateUserInfo"
      onFinish={onFinish}
      scrollToFirstError
      layout="vertical"
      initialValues={currentUser}
      style={{ maxWidth: 350 }}
    >
      <Form.Item name="avatarUrl">
        <UploadImage />
      </Form.Item>
      <Form.Item
        name="name"
        label="Họ và tên"
        rules={[
          {
            required: true,
            message: 'Vui lòng nhập họ và tên',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Cập nhật
        </Button>
      </Form.Item>
    </Form>
  );
}
