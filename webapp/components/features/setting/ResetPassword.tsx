import { Form, Input, Button } from 'antd';

import { formUtils } from '@/utils/form-utils';

export default function ResetPassword() {
  return (
    <Form
      name="resetPassword"
      onFinish={(values: any) => {
        formUtils.submitForm({
          method: 'PUT',
          endpoint: '/auth/reset-password',
          notify: true,
          modifyDataBeforeSubmit: (dataBeforeSubmit) => ({ password: dataBeforeSubmit.password }),
        }, values);
      }}
      style={{ maxWidth: 350 }}
      scrollToFirstError
      layout="vertical"
    >
      <Form.Item
        name="password"
        label="Mật khẩu"
        rules={[
          {
            required: true,
            message: 'Vui lòng nhập mật khẩu!',
          },
          {
            min: 6,
            message: 'Mật khẩu phải có ít nhất 6 kí tự',
          },
        ]}
        hasFeedback
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        name="rePassword"
        label="Xác nhận mật khẩu"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Vui lòng xác nhận mật khẩu!',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('Mật khẩu xác nhận không chính xác!'));
            },
          }),
        ]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Cập nhật
        </Button>
      </Form.Item>
    </Form>
  );
}
