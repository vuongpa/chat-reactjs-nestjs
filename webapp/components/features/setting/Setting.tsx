import { Tabs } from 'antd';
import React from 'react';

import UpdateUser from './UpdateUser';
import ResetPassword from './ResetPassword';
import styles from './Setting.module.scss';

export default function Setting() {
  return (
    <Tabs
      className={styles.settings}
      defaultActiveKey="user-info"
      items={[
        { label: 'Thông tin cá nhân', key: 'user-info', children: <UpdateUser /> },
        { label: 'Đổi mật khẩu', key: 'reset-password', children: <ResetPassword /> },
      ]}
    />
  );
}
