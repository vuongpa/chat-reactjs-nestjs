import { Button } from 'antd';
import { useDispatch } from 'react-redux';
import { PlusOutlined, SendOutlined } from '@ant-design/icons';

import { setUserIdInChat } from '../../message/conversation/redux/slice';

import { setShowModalAddFriend } from './redux/slice';

import OpacityClick from '@/components/atoms/opacity-click/OpacityClick';

export function AddFriend() {
  return (
    <OpacityClick>
      <PlusOutlined />
    </OpacityClick>
  );
}

export function SendMessage({ user }: {user: any}) {
  const dispatch = useDispatch();

  const sendMessage = () => {
    dispatch(setUserIdInChat(user._id));
    dispatch(setShowModalAddFriend(false));
  };

  return (
    <OpacityClick onClick={sendMessage}>
      <SendOutlined />
    </OpacityClick>
  );
}
