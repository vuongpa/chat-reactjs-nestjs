import { Avatar, Typography } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useState } from 'react';

import styles from './AddContact.module.scss';
import { AddFriend, SendMessage } from './ContactAction';

interface ResultContactItemProps {
  user: any;
}

export default function ResultContactItem({ user }: ResultContactItemProps) {
  const [mouseEnter, setMouseEnter] = useState<boolean>(false);

  return (
    <div
      onMouseEnter={() => setMouseEnter(true)}
      onMouseLeave={() => setMouseEnter(false)}
      className={styles['contact-item']}
    >
      <div className={styles.user}>
        <Avatar icon={<UserOutlined />} src={user?.avatarUrl} />
        <div>
          <Typography.Text>
            {user?.name}
            <Typography.Text type="secondary">{` (${user?.username})`}</Typography.Text>
          </Typography.Text>
        </div>
      </div>
      {mouseEnter && (
      <div className={styles['contact-action']}>
        <SendMessage user={user} />
        <AddFriend />
      </div>
      )}
    </div>
  );
}
