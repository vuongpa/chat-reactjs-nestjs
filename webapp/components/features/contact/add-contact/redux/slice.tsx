import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

interface IAddContactState {
  showModalAddFriend: boolean;
}

const initialState: IAddContactState = {
  showModalAddFriend: false,
};

export const addContactSlice = createSlice({
  name: 'addContact',
  initialState,
  reducers: {
    setShowModalAddFriend(state, action) {
      state.showModalAddFriend = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => ({
      ...state,
      ...action.payload.addContact,
    }),
  },
});

export const { setShowModalAddFriend } = addContactSlice.actions;
