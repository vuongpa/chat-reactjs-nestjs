import { SearchOutlined, UserAddOutlined } from '@ant-design/icons';
import { useEffect, useRef, useState } from 'react';
import { Empty, Input, Modal } from 'antd';
import { debounce } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';

import styles from './AddContact.module.scss';
import ResultContactItem from './ResultContactItem';
import { setShowModalAddFriend } from './redux/slice';

import OpacityClick from '@/components/atoms/opacity-click/OpacityClick';
import { formUtils } from '@/utils/form-utils';
import AxiosRequest from '@/utils/request-utils';
import { AppState } from '@/app/store';

export default function AddContact() {
  const dispatch = useDispatch();
  const showModal = useSelector((state: AppState) => state.addContact.showModalAddFriend);
  const inputRef = useRef<any>(null);

  const [contacts, setContacts] = useState<any>([]);

  const showingModal = () => dispatch(setShowModalAddFriend(true));

  const closeModal = () => dispatch(setShowModalAddFriend(false));

  const onSearch = debounce(async (e) => {
    const filter = {
      _q: e.target.value,
    };
    const response = await AxiosRequest.get(formUtils.endpointWithQuery(`/users/search?filter=${JSON.stringify(filter)}`));
    setContacts(response);
  }, 300);

  useEffect(() => inputRef?.current?.focus(), [showModal]);

  return (
    <div>
      <OpacityClick onClick={showingModal}>
        <UserAddOutlined />
      </OpacityClick>
      <Modal
        open={showModal}
        destroyOnClose
        onCancel={closeModal}
        title="Thêm bạn"
        footer={null}
      >
        <Input
          ref={inputRef}
          placeholder="Tìm kiếm"
          onChange={onSearch}
          prefix={<SearchOutlined />}
        />
        <div className={styles.result}>
          {contacts.length ? contacts.map((contact: any) => (
            <ResultContactItem
              key={contact._id}
              user={contact}
            />
          )) : <Empty />}
        </div>
      </Modal>
    </div>
  );
}
