import { UserOutlined } from '@ant-design/icons';
import { Popover, Avatar } from 'antd';
import { useDispatch } from 'react-redux';

import styles from './AvatarOption.module.scss';

import OpacityClick from '@/components/atoms/opacity-click/OpacityClick';
import { useCurrentUser } from '@/app/auth/hooks';
import { authActions } from '@/app/auth/slice';

export default function AvatarWithOption() {
  const currentUser = useCurrentUser();
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(authActions.logout());
  };

  return (
    <Popover
      content={(
        <div className={styles['avatar-options']}>
          <OpacityClick onClick={logout}>Đăng xuất</OpacityClick>
        </div>
      )}
      trigger="click"
      arrow={false}
      placement="right"
    >
      <Avatar icon={<UserOutlined />} src={currentUser?.avatarUrl} />
    </Popover>
  );
}
