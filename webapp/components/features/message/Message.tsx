/* eslint-disable jsx-a11y/alt-text */
import { Layout, Typography } from 'antd';

import styles from './Message.module.scss';
import HeaderSider from './sider/HeaderSider';
import HeaderContent from './content/HeaderContent';
import MainContent from './content/MainContent';
import MainSider from './sider/MainSider';
import { useUserInChat } from './conversation/redux/hooks';

const { Sider, Content } = Layout;

export default function Message() {
  const userInChat = useUserInChat();
  return (
    <Layout className={styles.message}>
      <Sider width={300} className={styles.sider} theme="light">
        <HeaderSider />
        <MainSider />
      </Sider>
      <Layout>
        {userInChat ? (
          <Content className={styles.content}>
            <HeaderContent />
            <MainContent />
          </Content>
        ) : (
          <div className={styles.welcome}>
            <Typography.Title level={3}>Chào mừng đến với trò chuyện</Typography.Title>
            <Typography.Text>Khám phá những tiện ích hỗ trợ làm việc và trò chuyện cùng người thân, bạn bè được tối ưu hoá cho máy tính của bạn.</Typography.Text>
            <br />
            <br />
            <br />
            <img className={styles['img-welcome']} src="https://chat.zalo.me/assets/quick-message-onboard.3950179c175f636e91e3169b65d1b3e2.png" />
          </div>
        )}
      </Layout>
    </Layout>
  );
}
