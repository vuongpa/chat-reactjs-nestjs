import { UsergroupAddOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { useState } from 'react';

import OpacityClick from '@/components/atoms/opacity-click/OpacityClick';

export default function ConversationGroup() {
  const [showModal, setShowModal] = useState<boolean>();

  const showingModal = () => setShowModal(true);

  const closeModal = () => setShowModal(false);

  return (
    <div>
      <OpacityClick onClick={showingModal}>
        <UsergroupAddOutlined />
      </OpacityClick>
      <Modal
        open={showModal}
        destroyOnClose
        onCancel={closeModal}
        okText="Tạo nhóm"
        cancelText="Hủy"
        title="Tạo nhóm mới"
      />
    </div>
  );
}
