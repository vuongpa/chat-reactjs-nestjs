import AddContact from '../../contact/add-contact/AddContact';
import ConversationGroup from '../conversation-group/ConversationGroup';
import SearchConversation, { CloseSearchConversation } from '../search-conversation/SearchConversation';
import { useSearchingConversation } from '../search-conversation/redux/hooks';
import styles from '../Message.module.scss';

export default function HeaderSider() {
  const searching = useSearchingConversation();
  return (
    <div className={styles['header-sider']}>
      <SearchConversation />
      {searching && <CloseSearchConversation />}
      {!searching && (
      <>
        <AddContact />
        <ConversationGroup />
      </>
      )}
    </div>
  );
}
