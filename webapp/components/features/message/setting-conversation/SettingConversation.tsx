import styles from './SettingConversation.module.scss';

export default function SettingConversation() {
  return (
    <div className={styles['setting-conversation']}>
      Setting Conversation
    </div>
  );
}
