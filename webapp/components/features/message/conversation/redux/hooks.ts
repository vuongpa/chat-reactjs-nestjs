import { useDispatch, useSelector } from 'react-redux';

import { AppState } from '@/store/store';
import { selectConversationList } from '@/app/socket/slice';

export function useUserIdInChat() {
  return useSelector((state: AppState) => state.conversation.userIdInChat);
}

export function useUserInChat() {
  return useSelector((state: AppState) => state.conversation.userInChat);
}

export function useMessagesInChat() {
  return useSelector((state: AppState) => state.conversation.messagesInChat);
}

export function useConversations() {
  return useSelector(selectConversationList);
}
