import { call, put, takeLatest } from 'redux-saga/effects';
import { ResponseGenerator } from 'next/dist/server/response-cache';

import {
  fetchConversations, fetchMessagesInChat, setConversations, setMessagesInChat, setUserIdInChat, setUserInChat,
} from './slice';

import AxiosRequest from '@/utils/request-utils';
import { formUtils } from '@/utils/form-utils';

function* fetchConversation() {
  try {
    const response: ResponseGenerator = yield call(
      AxiosRequest.get,
      formUtils.endpointWithQuery('/users/conversation'),
    );
    yield put(setConversations(response));
  } catch (error) {
    console.error('fetch me error: ', error);
  }
}

function* fetchUserInChat({ payload: userId }: any) {
  try {
    const response: ResponseGenerator = yield call(
      AxiosRequest.get,
      formUtils.endpointWithQuery(`/users/${userId}`),
    );
    yield put(setUserInChat(response));
  } catch (error) {
    console.error('fetch user in chat error: ', error);
  }
}

function* fetchMessageInChat({ payload: userId }: any) {
  try {
    const filter = {
      participantId: userId,
    };
    const messages: ResponseGenerator = yield call(
      AxiosRequest.get,
      formUtils.endpointWithQuery(`/messages?filter=${JSON.stringify(filter)}`),
    );
    yield put(setMessagesInChat(messages));
  } catch (error) {
    console.error('fetch user in chat error: ', error);
  }
}

export default function* conversationSaga() {
  yield takeLatest(fetchConversations, fetchConversation);
  yield takeLatest(setUserIdInChat, fetchUserInChat);
  yield takeLatest(setUserIdInChat, fetchMessageInChat);
  yield takeLatest(fetchMessagesInChat, fetchMessageInChat);
}
