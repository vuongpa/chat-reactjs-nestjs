import { createAction, createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import { AnyObject } from '@/utils/common/type';

export const fetchConversations = createAction('FETCH_CONVERSATIONS');
export const fetchMessagesInChat: any = createAction('FETCH_MESSAGES_IN_CHAT');

interface IConversationState {
  userIdInChat?: string;
  conversations: AnyObject[];
  userInChat?: AnyObject;
  messagesInChat: AnyObject[];
}

const initialState: IConversationState = {
  conversations: [],
  messagesInChat: [],
};

export const conversationSlice = createSlice({
  name: 'conversation',
  initialState,
  reducers: {
    setUserIdInChat(state, action) {
      state.userIdInChat = action.payload;
    },
    setConversations(state, action) {
      state.conversations = action.payload;
    },
    setUserInChat(state, action) {
      state.userInChat = action.payload;
    },
    setMessagesInChat(state, action) {
      state.messagesInChat = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => ({
      ...state,
      ...action.payload.conversation,
    }),
  },
});

export const {
  setUserIdInChat, setConversations, setUserInChat, setMessagesInChat,
} = conversationSlice.actions;
