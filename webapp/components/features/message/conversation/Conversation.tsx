import classNames from 'classnames';
import { Avatar, Typography } from 'antd';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { UserOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';

import styles from './Conversation.module.scss';
import { useUserIdInChat } from './redux/hooks';
import { setUserIdInChat } from './redux/slice';

import { useCurrentUserId } from '@/app/auth/hooks';
import { AnyObject } from '@/utils/type';

interface Props {
  name: string;
  avatarUrl?: string;
  lastMessage: string;
  senderName: string;
  lastTime: string;
  receiver?: AnyObject;
  sender?: AnyObject;
}

export default function Conversation({
  name, avatarUrl, lastMessage, senderName, lastTime, receiver, sender,
}: Props) {
  // Todo: menu options like: block, delete conversation
  const [mouseEnter, setMouseEnter] = useState<boolean>(false);
  const userIdInChat = useUserIdInChat();
  const dispatch = useDispatch();
  const isSelected = userIdInChat && [receiver?._id, receiver?._id].includes(userIdInChat);
  const currentUserId = useCurrentUserId();

  const onMouseEnter = () => {
    setMouseEnter(true);
  };

  const onMouseLeave = () => {
    setMouseEnter(false);
  };

  const goToChat = () => {
    const newUserIdInChat = currentUserId === sender?._id ? receiver?._id : sender?._id;
    dispatch(setUserIdInChat(newUserIdInChat));
  };

  useEffect(() => () => setMouseEnter(false), []);

  return (
    <div
      className={classNames(styles.conversation, {
        [styles.selected]: isSelected,
      })}
      onMouseLeave={onMouseLeave}
      onMouseEnter={onMouseEnter}
      onClick={goToChat}
      onKeyDown={goToChat}
    >
      <Avatar icon={<UserOutlined />} className={styles.avatar} size="large" src={avatarUrl} />
      <div className={styles['preview-conversation']}>
        <Typography.Paragraph className={styles.name}>{name}</Typography.Paragraph>
        <Typography.Paragraph className={styles.message} type="secondary">{`${senderName}: ${lastMessage}`}</Typography.Paragraph>
        <Typography.Text type="secondary" className={styles.time}>
          {moment(lastTime).format('DD/MM')}
        </Typography.Text>
      </div>
    </div>
  );
}
