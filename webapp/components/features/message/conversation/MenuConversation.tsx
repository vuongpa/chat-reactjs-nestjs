import { MoreOutlined } from '@ant-design/icons';
import { Popover, Typography } from 'antd';

import styles from './Conversation.module.scss';

function Content() {
  return (
    <div>content</div>
  );
}

export default function MenuConversation() {
  return (
    <div>
      <Popover className={styles['menu-conversation']} trigger="click" content={<Content />}>
        <Typography.Text type="secondary">
          <MoreOutlined width={10} height={20} />
        </Typography.Text>
      </Popover>
    </div>
  );
}
