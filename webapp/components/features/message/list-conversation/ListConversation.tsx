import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Empty } from 'antd';

import Conversation from '../conversation/Conversation';
import { useConversations } from '../conversation/redux/hooks';
import { useSearchConversationResult, useSearchingConversation } from '../search-conversation/redux/hooks';
import { fetchConversations } from '../conversation/redux/slice';

import styles from './ListConversation.module.scss';

import { useCurrentUser } from '@/app/auth/hooks';
import { Conversation as IConversation } from '@/models/message';

export default function ListConversation() {
  const currentUser = useCurrentUser();
  const searchingConversation = useSearchingConversation();
  const searchConversationResult = useSearchConversationResult();
  const conversations = useConversations();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchConversations());
  }, []);

  if (!searchingConversation) {
    return conversations.map((conversation: any) => {
      const currentUserIsSender = conversation?.sender?._id === currentUser?._id;
      const senderName = currentUserIsSender ? 'Bạn' : conversation?.sender?.name;
      const avatarUrl = currentUserIsSender ? conversation?.receiver?.avatarUrl : conversation?.sender?.avatarUrl;
      const participantName = currentUserIsSender ? conversation?.receiver?.name : conversation?.sender?.name;

      return (
        <Conversation
          key={conversation._id}
          name={participantName}
          avatarUrl={avatarUrl}
          lastMessage={conversation?.text}
          lastTime={conversation?.createdAt}
          senderName={senderName}
          receiver={conversation.receiver}
          sender={conversation.sender}
        />
      );
    });
  }

  return (
    <div className={styles['search-result']}>
      {searchConversationResult.length ? searchConversationResult.map((searchConversation: any) => (
        <Conversation
          key={searchConversation._id}
          avatarUrl={searchConversation.avatarUrl}
          name={searchConversation.name}
          lastMessage="Đi ẻ không hả mầy mầy mầy mầy"
          senderName="Bạn"
          lastTime={new Date().toISOString()}
        />
      )) : <Empty />}
    </div>
  );
}
