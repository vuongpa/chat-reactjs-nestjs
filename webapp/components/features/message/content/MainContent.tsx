import React, { useMemo } from 'react';

import { useMessagesInChat } from '../conversation/redux/hooks';

import styles from './Content.module.scss';

import ChatList from '@/components/atoms/chat-list/ChatList';
import InputChat from '@/components/atoms/chat-list/input-chat/InputChat';
import { ChatItemProps } from '@/components/atoms/chat-item/ChatItem';
import { useCurrentUser } from '@/app/auth/hooks';

export default function MainContent() {
  const currentUser = useCurrentUser();
  const messagesInChat = useMessagesInChat();

  const dataSource = useMemo(() => messagesInChat.map((message: any, index: number): ChatItemProps => {
    const currentUserIsSender = message?.sender?._id === currentUser?._id;
    const currentPosition = currentUserIsSender ? 'right' : 'left';
    const showAvatar = ((messagesInChat?.[index - 1]?.sender?._id === currentUser?._id) ? 'right' : 'left') !== currentPosition;

    return {
      id: message?._id,
      position: currentPosition,
      time: message?.createdAt,
      status: 'read',
      avatarUrl: message?.sender?.avatarUrl,
      message: message.text,
      showAvatar,
    };
  }), [messagesInChat]);

  return (
    <div className={styles['main-content']}>
      <ChatList dataSource={dataSource} />
      <InputChat />
    </div>
  );
}
