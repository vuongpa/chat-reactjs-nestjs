import {
  Avatar, Drawer, Empty, Typography,
} from 'antd';
import moment from 'moment';
import {
  InfoCircleOutlined,
  PhoneOutlined, UserOutlined, VideoCameraOutlined,
} from '@ant-design/icons';
import { useState } from 'react';

import { useUserIdInChat, useUserInChat } from '../conversation/redux/hooks';
import SettingConversation from '../setting-conversation/SettingConversation';

import styles from './Content.module.scss';

import OpacityClick from '@/components/atoms/opacity-click/OpacityClick';

export default function HeaderContent() {
  const [showSetting, setShowSetting] = useState<boolean>(false);
  const userInChat = useUserInChat();

  return (
    <div className={styles['header-content']}>
      <div className={styles.receiver}>
        <Avatar className={styles.avatar} size="large" icon={<UserOutlined />} />
        <div className={styles['preview-conversation']}>
          <Typography.Title level={5} className={styles.name}>{userInChat?.name}</Typography.Title>
          <Typography.Paragraph
            className={styles.message}
            type="secondary"
          >
            {`Truy cập ${moment(userInChat?.lastAccess).format('DD/MM')}`}
          </Typography.Paragraph>
        </div>
      </div>
      <div className={styles.action}>
        <OpacityClick>
          <PhoneOutlined style={{ fontSize: 20 }} />
        </OpacityClick>
        <OpacityClick>
          <VideoCameraOutlined style={{ fontSize: 20 }} />
        </OpacityClick>
        <OpacityClick onClick={() => setShowSetting(true)}>
          <InfoCircleOutlined style={{ fontSize: 20 }} />
        </OpacityClick>
      </div>
      <Drawer
        title="Thông tin hội thoại"
        destroyOnClose
        onClose={() => setShowSetting(false)}
        open={showSetting}
      >
        <SettingConversation />
      </Drawer>
    </div>
  );
}
