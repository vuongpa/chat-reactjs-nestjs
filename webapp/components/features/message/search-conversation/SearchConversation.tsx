import { SearchOutlined } from '@ant-design/icons';
import { Input } from 'antd';
import { debounce } from 'lodash';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

import styles from './SearchConversation.module.scss';
import { setSearchConversationResult, setSearchingConversation } from './redux/slice';

import OpacityClick from '@/components/atoms/opacity-click/OpacityClick';
import AxiosRequest from '@/utils/request-utils';
import { formUtils } from '@/utils/form-utils';

export function CloseSearchConversation() {
  const dispatch = useDispatch();

  return (
    <OpacityClick onClick={() => dispatch(setSearchingConversation(false))}>
      Đóng
    </OpacityClick>
  );
}

export default function SearchConversation() {
  const dispatch = useDispatch();

  const onSearch = debounce(async (e) => {
    const filter = {
      _q: e.target.value,
    };
    const response = await AxiosRequest.get(formUtils.endpointWithQuery(`/users/search?filter=${JSON.stringify(filter)}`));
    dispatch(setSearchConversationResult(response || []));
  }, 300);

  const onFocus = () => {
    dispatch(setSearchingConversation(true));
  };

  useEffect(() => () => {
    dispatch(setSearchingConversation(false));
  }, []);

  return (
    <div className={styles['search-conversation']}>
      <Input
        placeholder="Tìm kiếm"
        onFocus={onFocus}
        onChange={onSearch}
        prefix={<SearchOutlined />}
      />
    </div>
  );
}
