import { useSelector } from 'react-redux';

import { AppState } from '@/store/store';

export function useSearchingConversation() {
  return useSelector((state: AppState) => state.searchConversation.searching);
}

export function useSearchConversationResult() {
  return useSelector((state: AppState) => state.searchConversation.searchConversationResult);
}
