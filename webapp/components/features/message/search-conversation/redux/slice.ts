import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

import { AnyObject } from '@/utils/type';

export interface ISearchConversationState {
  searching: boolean;
  searchConversationResult: AnyObject[];
}

const initialState: ISearchConversationState = {
  searching: false,
  searchConversationResult: [],
};

export const searchConversationSlice = createSlice({
  name: 'searchConversation',
  initialState,
  reducers: {
    setSearchingConversation(state, action) {
      state.searching = action.payload;
    },
    setSearchConversationResult(state, action) {
      state.searchConversationResult = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => ({
      ...state,
      ...action.payload.searchConversation,
    }),
  },
});

export const { setSearchingConversation, setSearchConversationResult } = searchConversationSlice.actions;
