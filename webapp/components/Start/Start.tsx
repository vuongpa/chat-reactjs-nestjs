import {
  Button, Form, Input, Typography,
} from 'antd';
import React, { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import Link from 'next/link';

import Container from '../atoms/Container/Container';

import styles from './Start.module.scss';

import { formUtils } from '@/utils/form-utils';
import { AuthUtils } from '@/utils/auth.utils';
import { fetchMeAction } from '@/store/auth/saga';

export default function Start() {
  const inputRef = useRef<any>(null);
  const dispatch = useDispatch();
  const { push } = useRouter();

  const onFinish = (values: {username: string}) => {
    formUtils.submitForm({
      method: 'POST',
      endpoint: '/auth/start',
      notify: true,
      successMessage: 'Tạo tài khoản thành công',
      onGotSuccess: (data: any) => {
        AuthUtils.setAuthToken(data);
        dispatch(fetchMeAction());
        push('/');
      },
    }, values);
  };

  useEffect(() => {
    inputRef.current?.focus();
  }, []);

  return (
    <Container>
      <div className={styles.start}>
        <div className={styles.title}>
          <Typography.Title level={1}>
            What
            {'\''}
            s your name ?
          </Typography.Title>
          <Typography.Paragraph>Vui lòng nhập vào tên người dùng của bạn để bắt đầu sử dụng</Typography.Paragraph>
        </div>
        <Form
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="username"
            rules={[
              { required: true, message: 'Vui lòng nhập tên người dùng' },
              {
                pattern: /^[a-zA-Z0-9]{4,10}$/,
                message: 'Tên người dùng không được chứa ký tự đặc biệt',
              },
            ]}
          >
            <Input
              ref={inputRef}
              autoFocus
              className={styles.input}
              size="large"
              placeholder="Nhập vào tên người dùng"
            />
          </Form.Item>
          <Form.Item>
            <div className={styles.wrapperButton}>
              <Button htmlType="submit" className={styles.button} type="primary" size="large">Tiếp tục</Button>
            </div>
          </Form.Item>
          <div className={styles.login}>
            <Link href="/login">
              Đăng nhập tại đây -
              {'>'}
            </Link>
          </div>
        </Form>
      </div>
    </Container>
  );
}
