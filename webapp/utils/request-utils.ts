import axios from 'axios';
import Cookies from 'js-cookie';

import { AUTH_KEY } from './constants';
import { AuthUtils } from './auth.utils';

const AxiosRequest = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_DOMAIN,
  timeout: 5 * 60 * 1000,
  headers: {
    'Content-Type': 'application/json',
  },
  withCredentials: false,
});

// Make sure the request have latest access token
AxiosRequest.interceptors.request.use((config: any = {}) => ({
  ...config,
  headers: {
    ...config.header,
    Authorization: `Bearer ${Cookies.get(AUTH_KEY.ACCESS_TOKEN)}`,
  },
}), (error) => Promise.reject(error));

// Refresh accessToken when it expires
AxiosRequest.interceptors.response.use(
  (response) => response?.data,
  async (error) => {
    const originalRequest = error.config;
    if (error?.response?.status === 401 && !originalRequest?.retry) {
      originalRequest.retry = true;
      const authPayload: any = await AuthUtils.refreshAuthPayload();

      AuthUtils.setAuthToken(authPayload);

      AxiosRequest.defaults.headers.Authorization = `Bearer ${authPayload?.[AUTH_KEY.ACCESS_TOKEN]}`;
      originalRequest.headers.Authorization = `Bearer ${authPayload?.[AUTH_KEY.ACCESS_TOKEN]}`;

      return AxiosRequest(originalRequest);
    }

    return Promise.reject(error);
  },
);

export default AxiosRequest;
