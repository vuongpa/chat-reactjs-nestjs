import { User } from '@/models/user';

export interface IAuthPayload {
  accessToken: string;
  refreshToken: string;
  expiresIn: string;
  expiresAt: string;
  currentUser?: User;
}
