import Cookies from 'js-cookie';

import { AUTH_KEY } from './constants';
import { IAuthPayload } from './interfaces';

export const AuthUtils = {
  getAuthToken: () => ({
    accessToken: Cookies.get(AUTH_KEY.ACCESS_TOKEN),
    refreshToken: Cookies.get(AUTH_KEY.REFRESH_TOKEN),
  }),
  setAuthToken: ({
    accessToken, refreshToken, expiresAt, expiresIn,
  }: IAuthPayload) => {
    if (accessToken) Cookies.set(AUTH_KEY.ACCESS_TOKEN, accessToken);
    if (refreshToken) Cookies.set(AUTH_KEY.REFRESH_TOKEN, refreshToken);
    if (expiresAt) Cookies.set(AUTH_KEY.EXPIRES_IN, expiresAt);
    if (expiresIn) Cookies.set(AUTH_KEY.EXPIRES_AT, expiresIn);
  },
  refreshAuthPayload: async () => {
    const refreshToken = Cookies.get(AUTH_KEY.REFRESH_TOKEN);
    if (refreshToken) {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_DOMAIN}/auth/refresh`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${refreshToken}`,
        },
      });
      return response.json();
    }
    return null;
  },
  removeAllToken() {
    Cookies.remove(AUTH_KEY.ACCESS_TOKEN);
    Cookies.remove(AUTH_KEY.EXPIRES_AT);
    Cookies.remove(AUTH_KEY.EXPIRES_IN);
    Cookies.remove(AUTH_KEY.REFRESH_TOKEN);
  },
};
