export interface AnyObject<T = any> {
  [property: string]: any;
}
