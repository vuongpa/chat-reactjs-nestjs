import { useRouter } from 'next/router';

import { AuthUtils } from './auth.utils';

export function useLogout() {
  const { push } = useRouter();
  return () => {
    AuthUtils.removeAllToken();
    push('/login');
  };
}
