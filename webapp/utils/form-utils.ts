import { isEmpty } from 'lodash';
import { notification } from 'antd';

import { AnyObject } from './common/type';

import { messageUtils } from './message-utils';
import AxiosRequest from './request-utils';

export interface FormProps {
  endpoint: string;
  method: 'GET' | 'POST' | 'DELETE' | 'PUT';
  modifyDataBeforeSubmit?: (formValue: AnyObject) => AnyObject;
  beforeSubmit?: (formValue: AnyObject) => boolean;
  onGotError?: (error: AnyObject) => void;
  onGotSuccess?: (response: AnyObject) => void;
  successMessage?: string;
  errorMessage?: string;
  notify?: boolean;
  params?: AnyObject;
}

export const formUtils = {
  submitForm: (formProps: FormProps, submitData: AnyObject = {}) => {
    const {
      endpoint,
      method,
      modifyDataBeforeSubmit,
      beforeSubmit,
      onGotError,
      onGotSuccess,
      successMessage = messageUtils.successMessage[method as keyof typeof messageUtils.errorMessage],
      errorMessage,
      notify = false,
      params = {},
    } = formProps;

    const dataModifier = modifyDataBeforeSubmit?.(submitData) || submitData;
    if (beforeSubmit?.(dataModifier) ?? true) {
      AxiosRequest({
        method,
        data: dataModifier,
        url: formUtils.endpointWithQuery(endpoint, params),
      })
        .then((response: any) => {
          if (notify) { notification.success({ message: successMessage }); }
          onGotSuccess?.(response);
          return response;
        })
        .catch((err: any) => {
          if (notify) {
            notification.error({ message: err?.response?.data?.message || errorMessage || messageUtils.errorMessage[method as keyof typeof messageUtils.errorMessage] });
          }
          onGotError?.(err);
          return err;
        });
    }
    return null;
  },
  endpointWithQuery: (endpoint: string, params?: AnyObject) => {
    if (isEmpty(params)) {
      return endpoint;
    }

    return `${endpoint}?filter=${encodeURIComponent(JSON.stringify(params))}`;
  },
};
